const db = require('./db');
const logger = require('../util/logger');




function getSingleRetrivalResponses(req, res, next) {
  logger.winston.info('response.getSingleRetrivalResponses');

  db.query('select * from retrival_responses where retrival_response_id = ?', req.params.retrival_response_id, function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data[0]);
    }
  });
}

function getRetrivalIntentResponses(req, res, next) {
  logger.winston.info('response.getRetrivalIntentResponses');
  db.query('select * from retrival_responses where retrival_intent_id = ?  order by retrival_response_id desc', req.params.retrival_intent_id, function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data);
    }
  });
}

function getRetrivalIntentResponseQuery(req, res, next) {
  logger.winston.info('response.getRetrivalIntentResponseQuery');
  logger.winston.info(req.query.retrival_intent_ids);
  var array_intentIds = req.query.retrival_intent_ids.split(","); //Very hacky due to the node-sqlite not supporting IN from an array
  logger.winston.info(array_intentIds);
  db.query('select * from retrival_responses where retrival_intent_id in (' + array_intentIds + ')  order by retrival_response_id desc', function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data);
    }
  });
}

function createRetrivalIntentResponse(req, res, next) {
  logger.winston.info('response.createRetrivalIntentResponse');
  logger.winston.info(req.body.retrival_intent_id);
  logger.winston.info(req.body.response_text);
  db.query('insert into retrival_responses(retrival_intent_id, retrival_response_text)' + 'values (?,?)', [req.body.retrival_intent_id, req.body.response_text], function(err) {
    if (err) {
      logger.winston.error("Error inserting a new record");
    } else {
      res.status(200).json({ status: 'success', message: 'Inserted' });
    }
  });
}

function removeRetrivalResponse(req, res, next) {
  logger.winston.info('response.removeRetrivalResponse');
  logger.winston.warn(req.params.retrival_expression_id);
  db.query('delete from retrival_responses where retrival_response_id = ?', req.params.retrival_response_id, function(err) {
    if (err) {
      logger.winston.info("Error removing the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Removed' });
    }
  });
}

function updateRetrivalResponse(req, res, next) {
  logger.winston.info('response.updateRetrivalResponse');
  db.query('update retrival_responses set retrival_response_text = ? where retrival_response_id = ?', [req.body.retrival_response_text, req.body.retrival_response_id], function(err) {
    if (err) {
      logger.winston.error("Error updating the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Updated' });
    }
  });
}

module.exports = {
  getSingleRetrivalResponses,
  getRetrivalIntentResponses,
  createRetrivalIntentResponse,
  removeRetrivalResponse,
  getRetrivalIntentResponseQuery,
  updateRetrivalResponse};