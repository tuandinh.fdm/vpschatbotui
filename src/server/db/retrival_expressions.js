const db = require('./db');
const logger = require('../util/logger');

function getSingleRetrivalExpression(req, res, next) {
  logger.winston.info('expression.getSingleRetrivalExpression');

  db.query('select * from retrival_expressions where retrival_expression_id = ?', req.params.expression_id, function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data[0]);
    }
  });
}

function getRetrivalIntentExpressions(req, res, next) {
  logger.winston.info('expression.getRetrivalIntentExpressions');
  db.query('select * from retrival_expressions where retrival_intent_id = ?  order by retrival_expression_id desc', req.params.retrival_intent_id, function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data);
    }
  });
}

function getRetrivalIntentExpressionQuery(req, res, next) {
  logger.winston.info('expression.getRetrivalIntentExpressionQuery');
  var array_intentIds = req.query.retrival_intent_ids.split(","); //Very hacky due to the node-sqlite not supporting IN from an array
  logger.winston.info('select * from retrival_expressions where retrival_intent_id in (' + array_intentIds + ')  order by retrival_expression_id desc');
  db.query('select * from retrival_expressions where retrival_intent_id in (' + array_intentIds + ')  order by retrival_expression_id desc', function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data);
    }
  });
}

function createRetrivalIntentExpression(req, res, next) {
  logger.winston.info('expressions.createRetrivalIntentExpression');
  db.query('insert into retrival_expressions(retrival_intent_id, retrival_expression_text)' + 'values (?,?)', [req.body.retrival_intent_id, req.body.expression_text], function(err) {
    if (err) {
      logger.winston.error("Error inserting a new record");
    } else {
      res.status(200).json({ status: 'success', message: 'Inserted' });
    }
  });
}

function removeRetrivalExpression(req, res, next) {
  logger.winston.info('expressions.removeRetrivalExpression');
  logger.winston.warn(req.params.retrival_expression_id);
  db.query('delete from retrival_expressions where retrival_expression_id = ?', req.params.retrival_expression_id, function(err) {
    if (err) {
      logger.winston.info("Error removing the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Removed' });
    }
  });
}

function updateRetrivalExpression(req, res, next) {
  logger.winston.info('expressions.updateRetrivalExpression');
  db.query('update retrival_expressions set retrival_expression_text = ? where retrival_expression_id = ?', [req.body.expression_text, req.body.expression_id], function(err) {
    if (err) {
      logger.winston.error("Error updating the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Updated' });
    }
  });
}

module.exports = {
  getSingleRetrivalExpression,
  getRetrivalIntentExpressions,
  createRetrivalIntentExpression,
  removeRetrivalExpression,
  getRetrivalIntentExpressionQuery,
  updateRetrivalExpression};
