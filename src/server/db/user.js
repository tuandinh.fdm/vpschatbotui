//toan add multi user
const db = require('./db');
const logger = require('../util/logger');
function getUser(req, res, next) {
  logger.winston.info('user.getUser');
  db.query('select * from user where user_id = ?', req.params.user_id, function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data);
    }
  });
}
function createUser(req, res, next) {
  logger.winston.info('user.createUser');
  db.query('insert into user (user_id, user_name,password)' + 'values (?,?,?)', [req.body.user_id, req.body.user_name, req.body.password], function(err) {
    if (err) {
      logger.winston.error("Thêm mới user thất bại");
    } else {
      res.status(200).json({ status: 'success', message: 'Inserted' });
    }
  });
}
function updateUser(req, res, next) {
  logger.winston.info('user.updateUser');

  db.query('update user set password = ? where user_id = ?', [req.body.password, req.params.user_id], function(err) {
    if (err) {
      logger.winston.error("Error updating the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Updated' });
    }
  });
}
function removeUser(req, res, next) {
  //Remove all sub components of intent
  logger.winston.info('user.removeUser');

  db.query('delete from user where user_id = ?', req.params.user_id, function(err) {
    if (err) {
      logger.winston.error("Error removing the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Removed' });
    }
  });
}
module.exports = {
  getUser,
  createUser,
  updateUser,
  removeUser
};
