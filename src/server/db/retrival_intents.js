const db = require('./db');
const logger = require('../util/logger');

function getSingleRetrivalIntent(req, res, next) {
  logger.winston.info('retrival_intents.getSingleRetrivalIntents');
  db.query('select * from retrival_intents where retrival_intent_id = ?', req.params.retrival_intent_id, function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data[0]);
    }
  });
}

function getBotRetrivalIntents(req, res, next) {
  logger.winston.info('retrival_intents.getBotRetrivalIntents');
  db.query('select * from retrival_intents where bot_id = ? order by retrival_intent_id desc', req.params.bot_id, function(err, data) {
    if (err) {
      logger.winston.error(err);;
    } else {
      res.status(200).json(data);
    }
  });
}

function createBotRetrivalIntent(req, res, next) {
  logger.winston.info('retrival_intents.createBotRetrivalIntent');
  db.query('insert into retrival_intents (bot_id, retrival_name, retrival_intent_name)' + ' values (?,?,?)', [req.body.bot_id, req.body.retrival_name ,req.body.retrival_intent_name], function(err) {
    if (err) {
      logger.winston.error("Error inserting a new record");
    } else {
      res.status(200).json({ status: 'success', message: 'Inserted' });
    }
  });
}

function updateRetrivalIntent(req, res, next) {
  logger.winston.info('retrival_intents.updateRetrivalIntent');
  db.query('update retrival_intents set retrival_intent_name = ?, retrival_name = ? where retrival_intent_id = ?', [req.body.retrival_intent_name, req.body.retrival_name, req.params.retrival_intent_id], function(err) {
    if (err) {
      logger.winston.error("Error updating the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Updated' });
    }
  });
}

function removeRetrivalIntent(req, res, next) {
  //Remove all sub components of intent
  logger.winston.info('retrival_intents.removeRetrivalIntent');

  db.query('delete from retrival_intents where retrival_intent_id = ?', req.params.retrival_intent_id, function(err) {
    if (err) {
      logger.winston.error("Error removing the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Removed' });
    }
  });
}

module.exports = {
  getBotRetrivalIntents,
  createBotRetrivalIntent,
  getSingleRetrivalIntent,
  updateRetrivalIntent,
  removeRetrivalIntent
};
