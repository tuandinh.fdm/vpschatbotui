const mysql = require('mysql');
const logger = require('../util/logger');


var DB_HOST = process.env.db_host || 'localhost';
var DB_PORT = process.env.db_port || 10306;
//var db_file_path = "";
console.log(DB_HOST);
console.log(DB_PORT);

let conn = new mysql.createConnection({
  host: DB_HOST,
  port: DB_PORT,
  user: 'vps_schema',
  password: 'password',
  database: 'vps_schema'
});

conn.connect((err) => {
  if (err) {
    console.log('Database connect FAIL');
  } else {
    logger.winston.info('Database connected!');
    checkDBSchema();
  }
});
// var conn = mysql.createConnection({
//   host: DB_HOST,
//   port: DB_PORT,
//   user: 'vps_schema',
//   password: 'password',
//   database: 'vps_schema'
// });


// conn.connect((err) => {
//   if (err) throw err;
//   console.log('Kết nối database thành công!');
// });
//if (process.env.npm_lifecycle_event == 'test:server') {
//  db_file_path = "server/test/db/test_db.sqlite3";
//  try {
//    fs.unlinkSync(db_file_path);
//    //file removed
//  } catch(err) {
//    //console.error(err)
//  }
//} else {
//  db_file_path = "server/data/db.sqlite3";
//}

//let db = new sqlite3.Database(db_file_path, (err) => {
//  if (err) {
//    logger.winston.error('Error when connecting to the Database.', err)
//  } else {
//    logger.winston.info('Database connected!');
//    checkDBSchema();
//  }
//})

checkDBSchema();
function checkDBSchema() {
  //Get version of DB Schema from version table, if != to version, suggest upgrade
  conn.query("SELECT * from version", function (err, rows) {
    if (err && err.errno == 1) {

      createDBSchema();

    } else {
      if (rows.length > 0 && rows[0].version == global.db_schema) {
        logger.winston.info("Schema version v" + rows[0].version + " matches package.json schema version v" + global.db_schema);
        createDBSchema();
      } else {
        var current_version = "?";
        if (rows.length > 0) {
          current_version = rows[0].version;
        }
        logger.winston.warn("Schema version v" + current_version + " DOES NOT match package.json schema version v" + global.db_schema);
        if (global.db_autoupdate == "true") {
          createDBSchema();
        } else {
          logger.winston.error("Please upgrade your schema");
        }
      }
    }
  });
}

async function createDBSchema() {
  try {
    logger.winston.info("------------------------- Starting to create/update DB schema -------------------------");
    await Promise.all([
      // <<<<<<< HEAD
      //       db.run("CREATE TABLE  bots (bot_id INTEGER PRIMARY KEY AUTOINCREMENT, bot_name TEXT, bot_config TEXT, output_folder TEXT)", function(error) { sqlOutput(error, "bots"); }),
      //       db.run("CREATE TABLE  intents (intent_id INTEGER PRIMARY KEY AUTOINCREMENT, intent_name TEXT, bot_id INTEGER)", function(error) { sqlOutput(error, "intents"); }),
      //       db.run("CREATE TABLE  synonyms (synonym_id INTEGER PRIMARY KEY AUTOINCREMENT, synonym_reference TEXT, regex_pattern TEXT, bot_id INTEGER)", function(error) { sqlOutput(error, "synonyms"); }),
      //       db.run("CREATE TABLE  entities (entity_id INTEGER PRIMARY KEY AUTOINCREMENT, entity_name TEXT, slot_data_type TEXT, bot_id INTEGER)", function(error) { sqlOutput(error, "entities"); }),
      //       db.run("CREATE TABLE  expressions (expression_id INTEGER PRIMARY KEY AUTOINCREMENT, expression_text TEXT, intent_id INTEGER)", function(error) { sqlOutput(error, "expressions"); }),
      //       db.run("CREATE TABLE  expression_parameters (parameter_id INTEGER PRIMARY KEY AUTOINCREMENT, parameter_start INTEGER, parameter_end INTEGER, parameter_value TEXT, expression_id INTEGER, intent_id INTEGER, entity_id INTEGER)", function(error) { sqlOutput(error, "expression_parameters"); }),
      //       db.run("CREATE TABLE  regex (regex_id INTEGER PRIMARY KEY AUTOINCREMENT, regex_name TEXT, regex_pattern TEXT, bot_id INTEGER)", function(error) { sqlOutput(error, "regex"); }),
      //       db.run("CREATE TABLE  responses (response_id INTEGER PRIMARY KEY AUTOINCREMENT, response_text TEXT, response_type TEXT, action_id INTEGER)", function(error) { sqlOutput(error, "responses"); }),
      //       db.run("CREATE TABLE  synonym_variants (synonym_variant_id INTEGER PRIMARY KEY AUTOINCREMENT, synonym_value TEXT, synonym_id INTEGER)", function(error) { sqlOutput(error, "synonym_variants"); }),
      //       db.run("CREATE TABLE  nlu_log (log_id INTEGER PRIMARY KEY AUTOINCREMENT, ip_address TEXT, query TEXT, event_type TEXT, event_data TEXT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)", function(error) { sqlOutput(error, "nlu_log"); }),
      //       db.run("CREATE TABLE  models (model_id INTEGER PRIMARY KEY AUTOINCREMENT, model_name TEXT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP, comment TEXT, bot_id INTEGER, local_path TEXT, server_path TEXT, server_response TEXT)", function(error) { sqlOutput(error, "models"); }),
      //       db.run("CREATE TABLE  actions (action_id INTEGER PRIMARY KEY AUTOINCREMENT, action_name TEXT, bot_id INTEGER)", function(error) { sqlOutput(error, "actions"); }),
      //       db.run("CREATE TABLE  stories (story_id INTEGER PRIMARY KEY AUTOINCREMENT, story_name TEXT, story TEXT, bot_id INTEGER, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)", function(error) { sqlOutput(error, "stories"); }),

      //       // tuandh add faq
      //       db.run("CREATE TABLE retrival_intents (retrival_intent_id	INTEGER PRIMARY KEY AUTOINCREMENT, retrival_name	TEXT, retrival_intent_name TEXT, bot_id	INTEGER)", function(error) { sqlOutput(error, "retrival_intents"); }),

      //       //end
      //       db.run("CREATE TABLE  settings (setting_name TEXT, setting_value TEXT)", function(error) {
      // =======
      conn.query("CREATE TABLE  bots (bot_id INTEGER PRIMARY KEY AUTO_INCREMENT, bot_name TEXT, bot_config TEXT, output_folder TEXT)", function (error) { sqlOutput(error, "bots"); }),
      conn.query("CREATE TABLE  intents (intent_id INTEGER PRIMARY KEY AUTO_INCREMENT, intent_name TEXT, bot_id INTEGER)", function (error) { sqlOutput(error, "intents"); }),
      conn.query("CREATE TABLE  synonyms (synonym_id INTEGER PRIMARY KEY AUTO_INCREMENT, synonym_reference TEXT, regex_pattern TEXT, bot_id INTEGER)", function (error) { sqlOutput(error, "synonyms"); }),
      conn.query("CREATE TABLE  entities (entity_id INTEGER PRIMARY KEY AUTO_INCREMENT, entity_name TEXT, slot_data_type TEXT, bot_id INTEGER)", function (error) { sqlOutput(error, "entities"); }),
      conn.query("CREATE TABLE  expressions (expression_id INTEGER PRIMARY KEY AUTO_INCREMENT, expression_text TEXT, intent_id INTEGER)", function (error) { sqlOutput(error, "expressions"); }),
      conn.query("CREATE TABLE  expression_parameters (parameter_id INTEGER PRIMARY KEY AUTO_INCREMENT, parameter_start INTEGER, parameter_end INTEGER, parameter_value TEXT, expression_id INTEGER, intent_id INTEGER, entity_id INTEGER)", function (error) { sqlOutput(error, "expression_parameters"); }),
      conn.query("CREATE TABLE  regex (regex_id INTEGER PRIMARY KEY AUTO_INCREMENT, regex_name TEXT, regex_pattern TEXT, bot_id INTEGER)", function (error) { sqlOutput(error, "regex"); }),
      conn.query("CREATE TABLE  responses (response_id INTEGER PRIMARY KEY AUTO_INCREMENT, response_text TEXT, response_type TEXT, action_id INTEGER)", function (error) { sqlOutput(error, "responses"); }),
      conn.query("CREATE TABLE  synonym_variants (synonym_variant_id INTEGER PRIMARY KEY AUTO_INCREMENT, synonym_value TEXT, synonym_id INTEGER)", function (error) { sqlOutput(error, "synonym_variants"); }),
      conn.query("CREATE TABLE  nlu_log (log_id INTEGER PRIMARY KEY AUTO_INCREMENT, ip_address TEXT, query TEXT, event_type TEXT, event_data TEXT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)", function (error) { sqlOutput(error, "nlu_log"); }),
      conn.query("CREATE TABLE  models (model_id INTEGER PRIMARY KEY AUTO_INCREMENT, model_name TEXT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP, comment TEXT, bot_id INTEGER, local_path TEXT, server_path TEXT, server_response TEXT)", function (error) { sqlOutput(error, "models"); }),
      conn.query("CREATE TABLE  actions (action_id INTEGER PRIMARY KEY AUTO_INCREMENT, action_name TEXT, bot_id INTEGER)", function (error) { sqlOutput(error, "actions"); }),
      conn.query("CREATE TABLE  stories (story_id INTEGER PRIMARY KEY AUTO_INCREMENT, story_name TEXT, story TEXT, bot_id INTEGER, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)", function (error) { sqlOutput(error, "stories"); }),
      // tuandh
      conn.query("CREATE TABLE retrival_expressions (retrival_expression_id INTEGER PRIMARY KEY auto_increment, retrival_expression_text TEXT, retrival_intent_id INTEGER)", function (error) { sqlOutput(error, "retrival_expressions") }),
      conn.query("CREATE TABLE retrival_intents (retrival_intent_id	INTEGER auto_increment,retrival_name	TEXT,retrival_intent_name	TEXT,bot_id	INTEGER, PRIMARY KEY(retrival_intent_id))", function (error) { sqlOutput(error, "retrival_intents") }),
      conn.query("CREATE TABLE retrival_responses (retrival_response_id	INTEGER auto_increment,retrival_response_text	TEXT,retrival_intent_id	INTEGER,PRIMARY KEY(retrival_response_id))", function (error) { sqlOutput(error, "retrival_responses") }),
      // end
      conn.query("CREATE TABLE  settings (setting_name TEXT, setting_value TEXT)", function (error) {
        sqlOutput(error, "settings");
        conn.query("INSERT into settings (setting_name, setting_value) values ('refresh_time', '60000')");
      }),
      //Toan add table user
      conn.query("CREATE TABLE  user (user_id INTEGER PRIMARY KEY AUTO_INCREMENT, user_name TEXT, password TEXT)", function (error) { sqlOutput(error, "user"); }),
      //end add table user
      //New table part of Version 3.0.1
      conn.query("CREATE TABLE  conversations (conversation_id INTEGER PRIMARY KEY AUTO_INCREMENT, ip_address TEXT, conversation TEXT, story TEXT, bot_id INTEGER, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)", function (error) { sqlOutput(error, "conversations"); }),

      conn.query("CREATE TABLE  version(version)", function (error) { setDBSchemaVersion(error); })
    ]);
  } catch (err) {
    logger.winston.error(err);
  }
}

function sqlOutput(error, table_name) {
  if (!error) {
    logger.winston.info("Table: " + table_name + " created");
  }
}

function setDBSchemaVersion(error) {
  if (error) {
    conn.query("UPDATE version set version = ?", global.db_schema);
    logger.winston.info("Database Schema updated to v" + global.db_schema + " ");
  } else {
    conn.query("INSERT into version (version) values (?)", global.db_schema);
    logger.winston.info("Database Schema v" + global.db_schema + " created");
  }
}
module.exports = conn;
