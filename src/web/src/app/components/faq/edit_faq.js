angular.module('app').controller('EditFaqController', EditFaqController);


function EditFaqController($rootScope, $scope, $log, Bot, RetrivalIntent, RetrivalExpression, RetrivalExpressions, RetrivalResponse, RetrivalResponses) {
    Bot.get({ bot_id: $scope.$routeParams.bot_id }, function (data) {
        $scope.bot = data;
    });



    RetrivalIntent.get({ retrival_intent_id: $scope.$routeParams.retrival_intent_id }, function (data) {
        $scope.retrival_intent = data;
    });

    loadExpressions();
    loadResponses();

    function loadExpressions() {
        RetrivalExpressions.query({ retrival_intent_id: $scope.$routeParams.retrival_intent_id }, function (data) {
            $scope.expressionList = data;
        });
    }

    function loadResponses() {
        RetrivalResponses.query({ retrival_intent_id: $scope.$routeParams.retrival_intent_id }, function (data) {
            $scope.responseList = data;
        });
    }

    $scope.updateIntentNameAndWebhook = function (retrival_intent) {
        retrival_intent.retrival_name = 'faq';
        RetrivalIntent.update({ retrival_intent_id: retrival_intent.retrival_intent_id, retrival_name: retrival_intent.retrival_name }, retrival_intent).$promise.then(
            function () {
                $rootScope.$broadcast(
                    'setAlertText',
                    'Intent information updated Sucessfully!!'
                );
            }
        );
    };

    $scope.runExpression = function (expression_text) {
        $rootScope.$broadcast('executeTestRequest', expression_text);
    };

    $scope.deleteRetrivalIntent = function () {
        RetrivalIntent.remove({ retrival_intent_id: $scope.$routeParams.retrival_intent_id }).$promise.then(
            function () {
                $scope.go('/bot/' + $scope.$routeParams.bot_id);
            }
        );
    };

    $scope.addExpression = function () {
        const newObj = {};
        newObj.retrival_intent_id = $scope.$routeParams.retrival_intent_id;
        newObj.expression_text = this.expression_text;
        $log.info(newObj)
        RetrivalExpression.save(newObj).$promise.then(function () {
            $scope.expression_text = '';
            loadExpressions();
        });
    };

    $scope.addResponse = function () {
        const newObj = {};
        newObj.retrival_intent_id = $scope.$routeParams.retrival_intent_id;
        newObj.response_text = this.response_text;
        $log.info(newObj)
        RetrivalResponse.save(newObj).$promise.then(function () {
            $scope.response_text = '';
            loadResponses();
        });
    };

    $scope.deleteExpression = function (retrival_expression_id) {
        RetrivalExpression.remove({ retrival_expression_id: retrival_expression_id }).$promise.then(
            function () {
                loadExpressions();
            }
        );
    };

    $scope.deleteResponse = function (retrival_response_id) {
        RetrivalResponse.remove({ retrival_response_id: retrival_response_id }).$promise.then(
            function () {
                loadResponses();
            }
        );
    };
}
