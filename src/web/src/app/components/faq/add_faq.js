angular
.module('app')
.controller('AddFaqController', AddFaqController);


function AddFaqController($scope, Bot, RetrivalIntent) {
  Bot.get({bot_id: $scope.$routeParams.bot_id}, function(data) {
      $scope.bot = data;
  });

  $scope.addFaq = function(params) {
    this.formData.bot_id = $scope.$routeParams.bot_id;
    this.formData.retrival_name = 'faq'
    RetrivalIntent.save(this.formData).$promise.then(function(resp) {
      $scope.formData.retrival_intent_name = 'Retrival Name';
      $scope.go('/bot/' + $scope.bot.bot_id)
    });
  };
}
